<?php
/**
* Woocommerce Raiffeisen Gateway Class
* eCommerceConnect system (Visa/MasterCard) + OpenSSL
* about the system https://ecconnect.com.ua/en/about.html
*
* @author Eutelnet Team
* @copyright Copyright (c) 2015, EutelNet
* @license   http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
*/

// exit if accessed directly
if (!defined('ABSPATH')) { exit; }

function init_raiffeisen_gateway_class () {
    if (class_exists('WC_Payment_Gateway')) {
        class WC_Payment_Raiffeisen_Gateway extends WC_Payment_Gateway {
            public $currency_code = '941'; // RSD :: Serbian Dinar :: Serbia
            public $post_action_url = 'https://ecg.test.upc.ua/ecgtestsq/enter';

            /**
            * Constructor
            */
            public function __construct() {
                $this->id                   = 'raiffeisen';
                $this->icon                 = plugins_url('',__FILE__) .'/../assets/img/icon.png';
                $this->logo                 = plugins_url('',__FILE__) .'/../assets/img/logo.png';
                $this->logo_small           = plugins_url('',__FILE__) .'/../assets/img/logo_small.png';
                $this->order_button_text    = __('Proceed with Raiffeisen', 'wc_etl');
                $this->has_fields           = true;

                add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));

                // Load the form fields
                $this->init_form_fields();

                // Load the settings
                $this->init_settings();

                // Get setting values
				$this->title				= $this->get_option('title');
				$this->description			= $this->get_option('description');
				$this->email          		= $this->get_option('email');
            }

            /**
			* Initialise Gateway Settings Form Fields
			*/
			public function init_form_fields() {

				$this->form_fields = array(
					'enabled' => array(
						'title'			=> __('Enable/Disable', 'woocommerce'),
						'type'			=> 'checkbox',
						'label'			=> __('Enable Raiffeisen Payment', 'woocommerce'),
						'default'		=> 'yes'
					),
					'title' => array(
						'title'			=> __('Title', 'woocommerce'),
						'type'			=> 'text',
						'description'	=> __('This controls the title which the user sees during checkout.', 'woocommerce'),
						'default'		=> __('Raiffeisen Bank', 'woocommerce'),
						'desc_tip'		=> true,
					),
					'description' => array(
						'title'			=> __('Description', 'woocommerce'),
						'description'	=> __('This controls the description which the user sees during checkout.', 'woocommerce'),
						'type'			=> 'textarea',
						'default'		=> 'Online and Mobile Payment.'
					),
                    'merchant_id' => array(
                        'title'         => __('MerchantID', 'wc_etl'),
                        'type'          => 'text',
                        'default'       => ''
                    ),
                    'terminal_id' => array(
                        'title'         => __('TerminalID', 'wc_etl'),
                        'type'          => 'text',
                        'default'       => ''
                    ),
                    'currency_number' => array(
                        'title'         => __('ISO currency number', 'wc_etl'),
                        'type'          => 'number',
                        'description'   => __('ISO 3166-1 numeric code (list of codes <a href="http://data.okfn.org/data/core/currency-codes" target="_blank">http://data.okfn.org/data/core/currency-codes</a>, Active Codes section)', 'wc_etl'),
                        'default'       => ''
                    ),
                    'private_key' => array(
                        'title'         => __('Generated Private Key', 'wc_etl'),
                        'type'          => 'textarea',
                        'description'   => __('This is used for the signature of delivered data.', 'wc_etl'),
                        'default'       => ''
                    ),
                    'public_key' => array(
                        'title'         => __('Generated Public Key', 'wc_etl'),
                        'type'          => 'textarea',
                        'description'   => __('This is used for signature verification on transaction response.', 'wc_etl'),
                        'default'       => ''
                    ),
                    'test_mode' => array(
                        'title'         => __('Sandbox Enable/Disable', 'wc_etl'),
                        'type'          => 'checkbox',
                        'label'         => __('Enable Test payment mode', 'wc_etl'),
                        'default'       => 'no'
                    )
				);
			}

            /**
            * Submit payment
            */
            public function process_payment ($order_id) {
                $order = wc_get_order( $order_id );

                $MerchantID = $this->get_option('merchant_id');
                $TerminalID = $this->get_option('terminal_id');
                $postActionUrl = $this->get_option('test_mode') == 'yes' ? 'https://ecg.test.upc.ua/ecgtestsq/enter' : 'https://ecg.upc.ua/ecgq/go/';
                $PurchaseTime = date("ymdHis") ;
                $order_total = $order->get_total();
                $TotalAmount = $order_total*100;
                $CurrencyID = $this->get_option('currency_number');

                if (!empty($MerchantID) && !empty($TerminalID) && !empty($CurrencyID)) {

                    $ordered_items = $order->get_items();
                    foreach($ordered_items as $item) {
                        $ordered_items_array[] = $item['name'];
                    }
                    $ordered_items = implode(', ',$ordered_items_array);
                    $purchase_desc = date("F j, Y, g:i a") .' - '. $order->shipping_first_name .' '. $order->shipping_last_name .' : '. $ordered_items;
                    $session_id = session_id();

                    $data = "$MerchantID;$TerminalID;$PurchaseTime;$order_id;$CurrencyID;$TotalAmount;$session_id;";

                    $priv_key = $this->get_option('private_key');

                    $pkeyid = openssl_get_privatekey($priv_key);
                    openssl_sign( $data, $signature, $pkeyid );
                    openssl_free_key($pkeyid);

                    $b64sign = base64_encode($signature);
                    ?>
                    <html lang="sr-RS">
                        <head><title>Raiffeisen form</title></head>
                        <body>
                            <form id="forward-submit" action="<?php echo $postActionUrl ?>" method="POST">
                                <input name="Version" type="hidden" value="1" />
                                <input name="MerchantID" type="hidden" value="<?php echo $MerchantID ?>" />
                                <input name="TerminalID" type="hidden" value="<?php echo $TerminalID ?>" />
                                <input name="TotalAmount" type="hidden" value="<?php echo $TotalAmount ?>" />
                                <input name="SD" type="hidden" value="<?php echo $session_id ?>" />
                                <input name="Currency" type="hidden" value="<?php echo $CurrencyID ?>" />
                                <input name="locale" type="hidden" value="rs" />
                                <input name="PurchaseTime" type="hidden" value="<?php echo $PurchaseTime ?>" />
                                <input name="PurchaseDesc" type="hidden" value="<?php echo $purchase_desc ?>" />
                                <input name="OrderID" type="hidden" value="<?php echo $order_id ?>" />
                                <input name="Signature" type="hidden" value="<?php echo "$b64sign" ?>"/>
                            </form>
                            <script>
                                document.getElementById('forward-submit').submit();
                            </script>
                        </body>
                    </html>

                    <?php 

                } else {

                    echo 'Raiffeisen payment gateway is not properly configured.';

                }

            }


        } // end class declaration
    }
}
function add_raiffeisen_gateway_class ($methods) {
    $methods[] = 'WC_Payment_Raiffeisen_Gateway';
    return $methods;
}
