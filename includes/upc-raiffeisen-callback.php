<?php
class UPC_Raiffeisen_Callback {
    public static $_instance = null;
    public $response;
    public $gw; // paymentgateway

    public static function Instance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function __construct() {
        add_action('pre_get_posts', array($this, 'parse_response'));
    }

    public function parse_response ($query) {
        if ( !is_admin() && !empty($_POST) && isset($_POST['MerchantID']) ) {
            $this->gw = new WC_Payment_Raiffeisen_Gateway();
            $this->response = $_POST;

            // verification
            $signedFormData = $this->get_data_string($_POST);
            $this->verify_data($signedFormData);
        }
    }

    /**
    * Verification string getter
    * @param $p array
    * @return MerchantId;TerminalId;PurchaseTime;OrderId,Delay;Xid;CurrencyId,AltCurrencyId;Amount,AltAmount;SessionData;TranCode;ApprovalCode;
    */
    public function get_data_string($p) {
        $d = '';
        $d .= $this->gw->get_option('merchant_id') . ';';
        $d .= $this->gw->get_option('terminal_id') . ';';
        $d .= $p['PurchaseTime'] . ';';
        $d .= $p['OrderID'] . ',';
        $d .= $p['Delay'] . ';';
        $d .= $p['XID'] . ';';
        $d .= $p['Currency'] . ',';
        $d .= @$p['AltCurrency'] . ';';
        $d .= $p['TotalAmount'] . ',';
        $d .= @$p['AltTotalAmount'] . ';';
        $d .= $p['SD'] . ';';
        $d .= $p['TranCode'] . ';';
        $d .= $p['ApprovalCode'] . ';';

        return $d;
    }

    public function verify_data($formData) {

        $order_id = intval($this->response['OrderID']);
        $order = new WC_Order($order_id);

        if (is_null($order) || $order === false) {
            die('False order: #'. $this->response['OrderID']);
        }
        $redirectionUrl = $order->get_checkout_order_received_url();

        $signature = base64_decode($this->response['Signature']);
        $publicKeyId = openssl_get_publickey($this->gw->get_option('public_key'));
        $verified = openssl_verify($formData, $signature, $publicKeyId);

        // if ($verified !== 1) {
        //     var_dump($verified);
        // }

        if ($this->response['TranCode'] === '000') {
            // Complete the payment and reduce stock levels
            $order->payment_complete();

            // Remove cart
            if (!is_null(WC()->cart)) {
                WC()->cart->empty_cart();
            }

            ob_start(); ?>
            MerchantID=<?php echo $this->response['MerchantID'] ?>
            TerminalID=<?php echo $this->response['TerminalID'] ?>
            OrderID=<?php echo $this->response['OrderID'] ?>
            Currency=<?php echo $this->response['Currency'] ?>
            TotalAmount=<?php echo $this->response['TotalAmount'] ?>
            XID=<?php echo $this->response['XID'] ?>
            PurchaseTime=<?php echo $this->response['PurchaseTime'] ?>
            Response.action=<?php echo $this->response['MerchantID'] ?>
            Response.reason=<?php echo $this->response['MerchantID'] ?>
            Response.forwardUrl=<?php echo $redirectionUrl ?>

            <?php $notificationResponse = ob_get_clean();

            file_put_contents('purchase.log', date('m/d/Y h:i:s a') . ' Notification response: '. $notificationResponse ."\n", FILE_APPEND);
            wp_redirect($redirectionUrl);
            exit();
        } else {
            file_put_contents('purchase.log', date('m/d/Y h:i:s a') . ' Response code: <'. $this->response['TranCode'] .">\n", FILE_APPEND);
        }

        wp_redirect($redirectionUrl . '/#upc-code='. $this->response['TranCode']);
        exit();
    }

}

function UPC_Callback() {
    return UPC_Raiffeisen_Callback::Instance();
}
UPC_Callback();
