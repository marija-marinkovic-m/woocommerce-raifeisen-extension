<?php
/**
* Plugin Name: WooCommerce Raiffeisen
* Plugin URI: http://www.woothemes.com/woocommerce/
* Description: Used for Raiffeisen payments. eCommerce Connect is the system of accepting Visa и MasterCard payments online developed by Ukrainian Processing Center UPC in 2005 for.
* Author: EUTELNET Team
* Author URI: http://www.eutelnet.biz/
* Version: 1.0
* Text Domain: wc_etl
*
* Copyright: (c) 2016 eutelnet (marija@eutelnet.biz)
*
* License: GNU General Public License v3.0
* License URI: http://www.gnu.org/licenses/gpl-3.0.html
*
* @author    eutelnet team
* @copyright Copyright (c) 2016, EutelNet
* @license   http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
*
*/
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Check if WooCommerce is active
 */
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

	if ( ! class_exists( 'WC_Raiffeisen' ) ) {
        /**
		 * Localisation
		 **/
		load_plugin_textdomain( 'wc_etl', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

        class WC_Raiffeisen {
            /**
            * Singleton
            */
            protected static $_instance = null;

            /**
            * Main WC_Raiffeisen Instance
            * Ensures only one (singleton) Instance of class is loaded or can be loaded.
            * @static
            * @see WC_Raiffeisen_Instance()
            * @return WC_Raiffeisen - main instance
            */
            public static function instance() {
                if ( is_null(self::$_instance) ) {
                    self::$_instance = new self();
                }
                return self::$_instance;
            }

            private function __construct() {
                // requirements
                $this->includes();
                // action/filter hooks
                $this->hooks();
            }

            /**
			* Include required core files used in admin and on the frontend
			*/
            public function includes() {
                require('includes/class-wc-raiffeisen-gateway.php');
				require('includes/upc-raiffeisen-callback.php');
            }

            public function hooks() {
                // called only after woocommerce has finished loading
				add_action( 'woocommerce_init', array( &$this, 'woocommerce_loaded' ) );

				// called after all plugins have loaded
				add_action( 'plugins_loaded', array( &$this, 'plugins_loaded' ) );

                // add new payment Gateway
                add_filter( 'woocommerce_payment_gateways', 'add_raiffeisen_gateway_class' ); // class-wc-raiffeisen-gateway.php function

				// called just before the woocommerce template functions are included
				add_action( 'init', array( &$this, 'wp_init' ), 20 );

				// indicates we are running the admin
				if ( is_admin() ) {
					// add admin styles and scripts
					add_action('admin_enqueue_scripts', array( &$this, 'backend_scripts' ));
				} else {
					// add frontend styles and scripts
					add_action('wp_enqueue_scripts', array( &$this, 'frontend_scripts' ), 100, 99);
				}

				// indicates we are being served over ssl
				if ( is_ssl() ) {
					// ...
				}
            }

            /**
            * On WordPress init actions
            */
            public function wp_init () {
                // ...
            }

			/**
			 * Take care of anything that needs woocommerce to be loaded.
			 * For instance, if you need access to the $woocommerce global
			 */
			public function woocommerce_loaded() {
				// ...
			}

			/**
			 * Take care of anything that needs all plugins to be loaded
			 */
			public function plugins_loaded() {
				init_raiffeisen_gateway_class(); // class-wc-raiffeisen-gateway.php
			}

            /**
			* Load Backend styles and scripts method
			*/
			public function backend_scripts () {
				wp_enqueue_style('wc-etl-backend-styles', plugin_dir_url(__FILE__) . 'assets/css/backend.css');
				wp_enqueue_script('wc-etl-backend-script', plugin_dir_url(__FILE__) . 'assets/js/backend.js');
			}
			/**
			* Load Frontend styles and scripts
			*/
			public function frontend_scripts () {
				wp_enqueue_style('wc-etl-frontend-styles', plugin_dir_url(__FILE__) . 'assets/css/frontend.css');
				wp_enqueue_script('wc-etl-frontend-script', plugin_dir_url(__FILE__) . 'assets/js/frontend.js', array('jquery'), false, true);

				// @TODO: remove dequeue
				wp_dequeue_script( 'wc-checkout' );
			}

        } // end class declaration
    } // class doesn't exists

    /**
    * Returns the main instance of WC_Raiffeisen to prevent the need for Globals.
    * @return WC_Raiffeisen
    */
    function WC_Raiffeisen_Instance() {
        return WC_Raiffeisen::instance();
    }

    // run...
    WC_Raiffeisen_Instance();

} // woocommerce active
